package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/GetAllUsers.Feature",
                    "src/test/resources/CreatingNewUser.Feature"},
        glue={"StepDefinitions"}
)

public class Runner {

}