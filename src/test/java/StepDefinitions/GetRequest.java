package StepDefinitions;

import Config.ConfigurationReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import java.util.Collections;
import java.util.List;
import static io.restassured.RestAssured.*;
import static org.junit.Assert.assertEquals;

public class GetRequest {

    Functions functions = new Functions();
    public static Response response;
    String token = System.getProperty("tokenId");

    @Given("^Sending GET Request and verifying the created users information$")
    public void sendingGetRequestAndVerifyingUsers() {

        baseURI = ConfigurationReader.getProperty("baseUrl");

        response = given().log().all().contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when().log().all()
                .get(ConfigurationReader.getProperty("users"));

        response.then().body("data.email[0]", Matchers.equalTo("testHakanAutomation@gmail.com"));
        response.then().body("data.name[0]", Matchers.equalTo("testHakan"));
        response.then().body("data.gender[0]", Matchers.equalTo("male"));
        response.then().body("data.status[0]", Matchers.equalTo("active"));

    }

    @When("^Verifying expected status code and server information$")
    public void verifyingStatusCodeAndServer() {

        assertEquals(200, response.getStatusCode());
        response.then().header("Server", "cloudflare");

        System.out.println(response.then().extract().asString());

    }

    @Then("^Verifying that all data ids are four digit integers$")
    public void verifyingAllIds() {

        JsonPath responseJson = response.jsonPath();
        List<String> userIds = Collections.singletonList((responseJson.getString(("data.id"))));

        functions.findDigitNumbers(String.valueOf(userIds));
    }

}