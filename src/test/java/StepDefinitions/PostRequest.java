package StepDefinitions;

import Config.ConfigurationReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.aspectj.weaver.ast.Or;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import static io.restassured.RestAssured.*;
import static org.junit.Assert.assertEquals;


public class PostRequest {

    private Response response;
    String token = System.getProperty("tokenId");

    @Given("^Sending POST Request to users endpoint$")
    public void sendingPostRequest() {

        baseURI = ConfigurationReader.getProperty("baseUrl");

        response = given().log().all().contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .body(ConfigurationReader.getProperty("creatingNewUser"))
                .when().log().all()
                .post(ConfigurationReader.getProperty("users"));
    }

    @When("^Checking expected status code and server information$")
    public void checkingStatusCodeAndServer() {

        assertEquals(201, response.getStatusCode());
        response.then().header("Server", "cloudflare");
    }

    @Given("^Resending same POST request with same email address and verify that the user is not created$")
    public void resendingSamePostRequest() {

        Assert.assertEquals("data.message[0]",Matchers.equalTo("has already been taken"));
    }
}
